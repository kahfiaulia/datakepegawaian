<?php

    $url = 'https://hris.dev.azuralabs.id/api/auth/login/';
    $data = array("nip" => "0011");   
    $postdata = json_encode($data); 
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
    $result = curl_exec($ch);
    curl_close($ch);

    $login = json_decode($result, true);  
?>

!DOCTYPE html>
<html lang="en">
<html>
    <head>
    <?php $this->load->view("utama/_partials/head.php") ?>
    </head>
    <body class="sb-nav-fixed">
        <nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark">
            <a class="navbar-brand" href="<?php echo site_url('utama') ?>">Data Kepegawaian</a><button class="btn btn-link btn-sm order-1 order-lg-0" id="sidebarToggle" href="#"><i class="fas fa-bars"></i></button
            ><!-- Navbar Search-->
            <form class="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0">
                <div class="input-group">
                    <input class="form-control" type="text" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2" />
                    <div class="input-group-append">
                        <button class="btn btn-primary" type="button"><i class="fas fa-search"></i></button>
                    </div>
                </div>
            </form>
            <!-- Navbar-->
            <ul class="navbar-nav ml-auto ml-md-0">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" id="userDropdown" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-user fa-fw"></i></a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
                        <a class="dropdown-item" href="#">Settings</a><a class="dropdown-item" href="#">Activity Log</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="login.html">Logout</a>
                    </div>
                </li>
            </ul>
        </nav>
        <div id="layoutSidenav">
            <div id="layoutSidenav_nav">
                <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
                    <div class="sb-sidenav-menu">
                        <div class="nav">
                            <div class="sb-sidenav-menu-heading">Utama</div>
                            <a class="nav-link" href="<?php echo site_url('utama') ?>">
                                Data Diri Pegawai</a
                            >
                            <a class="nav-link" href="<?php echo site_url('pendidikan') ?>"
                                > Data Pendidikan</a>
                            <a class="nav-link" href="<?php echo site_url('pegawai') ?>"
                                >
                                Data Pegawai</a
                            >
                        </div>
                    </div>
                    <div class="sb-sidenav-footer">
                        <div class="small">Logged in as:</div>
                        Start Bootstrap
                    </div>
                </nav>
            </div>
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid">
                        <h1 class="mt-4">Data Diri Pegawai</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item active">Internship UNNES Azura Labs</li>
                        </ol>
                        <div class="row">
                        <table class="table table-striped ml-3">
                            <tbody>
                                    <tr>
                                        <th>ID: </th>
                                        <td><?php echo $login['data']['user']['id']?></td>
                                    </tr>
                                    <tr>
                                        <th>NIP: </th>
                                        <td><?php echo $login['data']['user']['nip']?></td>
                                    </tr>
                                    <tr>
                                        <th>NIK: </th>
                                        <td><?php echo $login['data']['user']['nik']?></td>
                                    </tr>
                                    <tr>
                                        <th>KTP: </th>
                                        <td><img src="<?php echo "https://hris.dev.azuralabs.id".$login['data']['user']['ktp']; ?>" alt="" class="img-thumbnail"></td>
                                    </tr>
                                    <tr>
                                        <th>Nama: </th>
                                        <td><?php echo $login['data']['user']['gelarDepan'].' '.$login['data']['user']['nama'].', '.$login['data']['user']['gelarBelakang']?></td>
                                    </tr>
                                    <tr>
                                        <th>Foto: </th>
                                        <td><img src="<?php echo "https://hris.dev.azuralabs.id".$login['data']['user']['foto']; ?>" alt="" class="img-thumbnail"></td>
                                    </tr>
                                    <tr>
                                        <th>Nama Panggilan: </th>
                                        <td><?php echo $login['data']['user']['namaPanggilan']?></td>
                                    </tr>
                                    <tr>
                                        <th>Tempat/Tanggal Lahir: </th>
                                        <td><?php echo $login['data']['user']['tempatLahir'].', '.$login['data']['user']['tanggalLahir']?></td>
                                    </tr>
                                    <tr>
                                        <th>Jenis Kelamin: </th>
                                        <td><?php echo $login['data']['user']['jenisKelamin']?></td>
                                    </tr>
                                    <tr>
                                        <th>Agama: </th>
                                        <td><?php echo $login['data']['user']['agama']?></td>
                                    </tr>
                                    <tr>
                                        <th>Alamat KTP: </th>
                                        <td><?php echo $login['data']['user']['alamatKTP']?></td>
                                    </tr>
                                    <tr>
                                        <th>Alamat Tinggal: </th>
                                        <td><?php echo $login['data']['user']['alamatTinggal']?></td></tr>
                                    <tr>
                                        <th>Email: </th>
                                        <td><?php echo $login['data']['user']['email']?></td>
                                    </tr>
                                    <tr>
                                        <th>Nomor Telepon: </th>
                                        <td><?php echo $login['data']['user']['phone']?></td></tr>
                                    <tr>
                                        <th>Nomor HP: </th>
                                        <td><?php echo $login['data']['user']['mobile']?></td>
                                    </tr>
                                    <tr>
                                        <th>Status Perkawinan: </th>
                                        <td><?php if ($login['data']['user']['statusPerkawinan'] = 1){
                                            echo "Menikah";
                                        }
                                        else{
                                            echo "Belum Menikah";
                                        }?></td>
                                    </tr>
                                    <tr>
                                        <th>Status Wajib Pajak: </th>
                                        <td><?php echo $login['data']['user']['statusWajibPajak']?></td>
                                    </tr>
                                    <tr>
                                        <th>Nomor KK: </th>
                                        <td><?php echo $login['data']['user']['kk']?></td>
                                    </tr>
                                    <tr>
                                        <th>Scan KK: </th>
                                        <td><img src="<?php echo "https://hris.dev.azuralabs.id".$login['data']['user']['scanKK']; ?>" alt="" class="img-thumbnail"></td>
                                    </tr> 
                                    <tr>
                                        <th>NPWP: </th>
                                        <td><?php echo $login['data']['user']['npwp']?></td>
                                    </tr>
                                    <tr>
                                        <th>Scan NPWP: </th>
                                        <td><img src="<?php echo "https://hris.dev.azuralabs.id".$login['data']['user']['scanNPWP']; ?>" alt="" class="img-thumbnail"></td>
                                    </tr>
                                    <tr>
                                        <th>Nomor BPJS Ketenagakerjaan: </th>
                                        <td><?php echo $login['data']['user']['bpjstk']?></td>
                                    </tr>
                                    <tr>
                                        <th>Nomor BPJS Kesehatan: </th>
                                        <td><?php echo $login['data']['user']['bpjskes']?></td>
                                    </tr>
                                    <tr>
                                        <th>Golongan Darah: </th>
                                        <td><?php echo $login['data']['user']['golonganDarah']?></td>
                                    </tr> 
                                    <tr>
                                        <th>Nomor Rekam Medis: </th>
                                        <td><?php echo $login['data']['user']['nomorRekamMedik']?></td>
                                    </tr> 
                                    <tr>
                                        <th>Nomor SIM: </th>
                                        <td><?php echo $login['data']['user']['nomorSIM'] ?></td>
                                    </tr>
                                    <tr>
                                        <th>Nomor Rekening: </th>
                                        <td><?php echo $login['data']['user']['nomorSIM']?></td>
                                    </tr> 
                                    <tr>
                                        <th>Golongan: </th>
                                        <td><?php echo $login['data']['user']['golongan']?></td>
                                    </tr>
                                    <tr>
                                        <th>Profesi: </th>
                                        <td><?php echo $login['data']['user']['profesi']?></td>
                                    </tr>
                                    <tr>
                                        <th>Jabatan: </th>
                                        <td><?php echo $login['data']['user']['jabatan'] ?></td>
                                    </tr>
                                    <tr>
                                        <th>Susunan Organisasi dan Tata Kerja: </th>
                                        <td><?php echo $login['data']['user']['sotk'] ?></td>
                                    </tr>
                                    <tr>
                                        <th>Gaji Pokok: </th>
                                        <td><?php echo $login['data']['user']['gapok'] ?></td>
                                    </tr>
                                    <tr>
                                        <th>Unit: </th>
                                        <td><?php echo $login['data']['user']['unit'] ?></td>
                                    </tr>
                                    <tr>
                                        <th>Status Kepegawaian: </th>
                                        <td><?php echo $login['data']['user']['statusKepegawaian'] ?></td>
                                    </tr>
                                    <tr>
                                        <th>Akta Kematian: </th>
                                        <td><?php echo $login['data']['user']['aktaKematian'] ?></td>
                                    </tr>
                                    <tr>
                                        <th>Tanggal Kematian: </th>
                                        <td><?php echo $login['data']['user']['tanggalKematian'] ?></td>
                                    </tr>
                                    <tr>
                                        <th>Tanggal Pensiun: </th>
                                        <td><?php echo $login['data']['user']['tanggalPensiun'] ?></td>
                                    </tr>
                                    <tr>
                                        <th>Jenjang Karir: </th>
                                        <td><?php echo $login['data']['user']['jenjangKarirId'] ?></td>
                                    </tr>     
                            </table>
                        </div>
                </main>
                <footer class="py-4 bg-light mt-auto">
                    <div class="container-fluid">
                        <div class="d-flex align-items-center justify-content-between small">
                            <div class="text-muted">Copyright &copy; Data Pegawai</div>
                            <div>
                                <a href="#">Privacy Policy</a>
                                &middot;
                                <a href="#">Terms &amp; Conditions</a>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
        <script src="https://code.jquery.com/jquery-3.4.1.min.js" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <script src="<?php echo base_url('js/scripts.js') ?>"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js" crossorigin="anonymous"></script>
        <script src="<?php echo base_url('assets/demo/chart-area-demo.js') ?>"></script>
        <script src="<?php echo base_url('assets/demo/chart-bar-demo.js') ?>"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
        <script src="<?php echo base_url('assets/demo/datatables-demo.js') ?>"></script>
    </body>
</html>

</head>

</html>
