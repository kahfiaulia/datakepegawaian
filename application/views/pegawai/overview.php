<?php
function http_request($url){
		$ch = curl_init(); 
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch,CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		$output = curl_exec($ch); 
		curl_close($ch);      
		return $output;
    }
    
    $pegawai = http_request("https://hris.dev.azuralabs.id/api/pegawai/");
    $pegawai = json_decode($pegawai, TRUE);
?>

<!DOCTYPE html>
<html lang="en">
<html>
    <head>
    <?php $this->load->view("pegawai/_partials/head.php") ?>
    </head>
    <body class="sb-nav-fixed">
        <nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark">
            <a class="navbar-brand" href="<?php echo site_url('utama') ?>">Data Kepegawaian</a><button class="btn btn-link btn-sm order-1 order-lg-0" id="sidebarToggle" href="#"><i class="fas fa-bars"></i></button
            ><!-- Navbar Search-->
            <form class="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0">
                <div class="input-group">
                    <input class="form-control" type="text" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2" />
                    <div class="input-group-append">
                        <button class="btn btn-primary" type="button"><i class="fas fa-search"></i></button>
                    </div>
                </div>
            </form>
            <!-- Navbar-->
            <ul class="navbar-nav ml-auto ml-md-0">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" id="userDropdown" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-user fa-fw"></i></a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
                        <a class="dropdown-item" href="#">Settings</a><a class="dropdown-item" href="#">Activity Log</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="login.html">Logout</a>
                    </div>
                </li>
            </ul>
        </nav>
        <div id="layoutSidenav">
            <div id="layoutSidenav_nav">
                <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
                    <div class="sb-sidenav-menu">
                        <div class="nav">
                            <div class="sb-sidenav-menu-heading">Utama</div>
                            <a class="nav-link" href="<?php echo site_url('utama') ?>">
                                Data Diri Pegawai</a
                            >
                            <a class="nav-link" href="<?php echo site_url('pendidikan') ?>"
                                > Data Pendidikan</a>
                            <a class="nav-link" href="<?php echo site_url('pegawai') ?>"
                                >
                                Data Pegawai</a
                            >
                        </div>
                    </div>
                    <div class="sb-sidenav-footer">
                        <div class="small">Logged in as:</div>
                        Start Bootstrap
                    </div>
                </nav>
            </div>
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid">
                        <h1 class="mt-4">Data Pegawai</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item active">Internship UNNES Azura Labs</li>
                        </ol>

                        <table class="table ml-1 table-responsive">
                        <div class="row">
                            <div class="col-md-4">
                                <input class="form-control mb-4 ml-1 input-sm" type="text" placeholder="Cari Data..." id="cariPegawai">
                            </div>
                            <button button type="button" class="btn btn-success ml-1 mb-4">Cari</button>
                            <div class="col">
                                <button button type="button" class="btn btn-primary ml-5 mb-4 float-right">Tambah Data</button>
                            </div>
                        </div>
                            <thead class="thead-dark text-center">
                                <tr>
                                <th scope="col">ID</th>
                                <th scope="col">NIP</th>
                                <th scope="col">NIK</th>
                                <th scope="col">Nama</th>
                                <th scope="col">Tempat/Tanggal Lahir</th>
                                <th scope="col">Jenis Kelamin</th>
                                <th scope="col">Alamat Tinggal</th>
                                <th scope="col">Nomor Telepon</th>
                                <th scope="col">Aksi</th>
                                </tr>
                            </thead>
                            <tbody id="dataPegawai">
                                <?php for($i=0; $i<count($pegawai['data']); $i++) { ?>
                                <tr>
                                    <td><?php echo $pegawai['data'][$i]['id']?></td>
                                    <td><?php echo $pegawai['data'][$i]['nip']?></td>
                                    <td><?php echo $pegawai['data'][$i]['nik']?></td>
                                    <td><?php echo $pegawai['data'][$i]['gelarDepan'].' '.$pegawai['data'][$i]['nama'].', '.$pegawai['data'][$i]['gelarBelakang']?></td>
                                    <td><?php echo $pegawai['data'][$i]['tempatLahir'].', '.$pegawai['data'][$i]['tanggalLahir']?></td>
                                    <td><?php echo $pegawai['data'][$i]['jenisKelamin']?></td>
                                    <td><?php echo $pegawai['data'][$i]['alamatTinggal']?></td>
                                    <td><?php echo $pegawai['data'][$i]['phone']?></td>
                                    <td class="btn-toolbar"><button type="button" class="btn btn-primary btn-sm btn-block">Detil</button><button type="button" class="btn btn-success btn-sm btn-block">Ubah</button><button type="button" class="btn btn-danger btn-sm btn-block">Hapus</button></td>
                                </tr>
                                <?php }?>
                            </tbody>
                            </table>
                        </div>
                </main>
                <footer class="py-4 bg-light mt-auto">
                    <div class="container-fluid">
                        <div class="d-flex align-items-center justify-content-between small">
                            <div class="text-muted">Copyright &copy; Data Pegawai</div>
                            <div>
                                <a href="#">Privacy Policy</a>
                                &middot;
                                <a href="#">Terms &amp; Conditions</a>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
        <script src="https://code.jquery.com/jquery-3.4.1.min.js" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <script src="<?php echo base_url('js/scripts.js') ?>"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js" crossorigin="anonymous"></script>
        <script src="<?php echo base_url('assets/demo/chart-area-demo.js') ?>"></script>
        <script src="<?php echo base_url('assets/demo/chart-bar-demo.js') ?>"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
        <script src="<?php echo base_url('assets/demo/datatables-demo.js') ?>"></script>
    </body>
</html>
</head>
</html>

<script>
$(document).ready(function(){
  $("#cariPegawai").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#dataPegawai tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>
